import 'package:flutter/material.dart';
import 'package:td_immo/models/habitation.dart';
import 'package:td_immo/share/location_style.dart';
import 'package:td_immo/share/location_text_style.dart';
import 'package:td_immo/views/share/habitation_features_widget.dart';

class HabitationDetails extends StatefulWidget {
  final Habitation _habitation;
  const HabitationDetails(this._habitation, {Key? key}) : super(key: key);

  @override
  State<HabitationDetails> createState() => _HabitationDetailsState();
}

class _HabitationDetailsState extends State<HabitationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._habitation.libelle),
      ),
      body: ListView(
        padding: EdgeInsets.all(4.0),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/locations/${widget._habitation.image}',
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            margin: EdgeInsets.all(8.0),
            child: Text(widget._habitation.adresse),
          ),
          HabitationFeaturesWidget(widget._habitation),
          _buildItems(),
          _buildOptionsPayantes(),
          _buildRentButton(),
        ],
      ),
    );
  }
}

_buildItems() {
  var width = (MediaQuery.of(context).size.width / 2) - 15;
  return Wrap(
    spacing: 2.0,
    children: Iterable.generate(
        widget._habitation.options.length,
        (i) => Container(
              padding: EdgeInsets.only(left: 15),
              margin: EdgeInsets.all(2),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget._habitation.libelle,
                    style:
                        TextStyle(fontWeight: LocationTextStyle.boldTextStyle),
                  ),
                  SizedBox(height: 8),
                  Text(
                    widget._habitation.description,
                    style: regularGreyTextStyle,
                  ),
                ],
              ),
            )),
  );
}

_buildRentButton() {
  var format = NumberFormat("### €");

  return Container(
    decoration: BoxDecoration(
        color: LocationStyle.bgPurple,
        borderRadius: BorderRadius.circular(8.0)),
    margin: EdgeInsets.symmetric(horizontal: 8.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              format.format(widget._habitation.prixmois),
            )),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 8.0),
          child: ElevatedButton(
            onPressed: () {
              print("Louer Habitation");
            },
            child: Text("Louer"),
          ),
        )
      ],
    ),
  );
}
