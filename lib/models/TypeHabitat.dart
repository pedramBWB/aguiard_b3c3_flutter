class TypeHabitat {
  int id;
  String libelle;

  TypeHabitat(this.id, this.libelle);

  fromJson(Map<String, dynamic> json) : id =json['id'], libelle = json['libelle'];
}